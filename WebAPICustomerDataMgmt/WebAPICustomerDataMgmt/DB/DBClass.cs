﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPICustomerDataMgmt.Models;
using WebAPICustomerDataMgmt.EntityDB;
using System.Data;
using System.ComponentModel;

namespace WebAPICustomerDataMgmt.DB
{
    public class DBClass
    {
        BIDCustomerDataEntities1 DBEntities = new BIDCustomerDataEntities1();
        public int SaveCustomer(CustomerModel obj)
        {
            int result = 0;
            try
            {
             
                Customer dbobj = new Customer();
                dbobj.AccountCode = obj.accountCode;
                dbobj.AddressInfo = obj.address;
                dbobj.AddressLine2 = obj.addressline2;
                dbobj.AddressLine3 = obj.addressline3;
                dbobj.County = obj.county;
                dbobj.CoRegisterationNo = obj.coRegisterationNo;
                //dbobj.DepotId = d;
                dbobj.GroupId = obj.groups;
                dbobj.StatusId = obj.status;
                dbobj.QueryStatusId = obj.querystatus;
                dbobj.DateRequested = (obj.dateRequested == "")? DateTime.Now : Convert.ToDateTime(obj.dateRequested);
                dbobj.RequestedBy = obj.requestedBy;
                //dbobj.ChangeDate = Convert.ToDateTime(obj.changeDate);
                dbobj.Notes = obj.notes;
                dbobj.TradingName = obj.tradingName;
                dbobj.LegalEntitty = obj.legalEntity;
                dbobj.PostCode = obj.postCode;

                DBEntities.Customers.Add(dbobj);
                result = DBEntities.SaveChanges();

                if (dbobj.Id > 0)
                {
                    if (obj.depots.Length > 0)
                    {
                        foreach (var d in obj.depots)
                        {
                            DepotCustomerMapping _obj = new DepotCustomerMapping();
                            _obj.CustomerId = dbobj.Id;
                            _obj.DepotId = d;

                            DBEntities.DepotCustomerMappings.Add(_obj);
                            DBEntities.SaveChanges();
                        }
                    }

                    CustomerAccountMapping _cust = new CustomerAccountMapping();
                    _cust.CustomerId = dbobj.Id;
                    _cust.AccountCode = dbobj.AccountCode;

                    DBEntities.CustomerAccountMappings.Add(_cust);
                    DBEntities.SaveChanges();

                }

                if (dbobj.Id != 0)
                {
                    AuditLog _obj = new AuditLog();
                    _obj.CustomerId = dbobj.Id;
                    _obj.LastChangeDescription = "Customer -" + dbobj.TradingName + "  has been created successfully";
                    _obj.ModificationDate = DateTime.Now;
                    _obj.ModifiedBy = 1;

                    DBEntities.AuditLogs.Add(_obj);
                    DBEntities.SaveChanges();

                }
               
            }
            catch (Exception ex)
            {
                AuditLog _obj = new AuditLog();
                //_obj.CustomerId = dbobj.Id;
                _obj.LastChangeDescription = "Application Error - " + Convert.ToString(ex);
                _obj.ModificationDate = DateTime.Now;
                _obj.ModifiedBy = 1;

                DBEntities.AuditLogs.Add(_obj);
                DBEntities.SaveChanges();
                throw (ex);
            }

            return result;
        }


        public int UpdateCustomer(CustomerModel obj)
        {
            int result = 0;
            try
            {
                Customer dbobj = new Customer();
                dbobj.AccountCode = obj.accountCode;
                dbobj.AddressInfo = obj.address;
                dbobj.AddressLine2 = obj.addressline2;
                dbobj.AddressLine3 = obj.addressline3;
                dbobj.County = obj.county;
                dbobj.CoRegisterationNo = obj.coRegisterationNo;
                //dbobj.DepotId = d;
                dbobj.GroupId = obj.groups;
                dbobj.StatusId = obj.status;
                dbobj.QueryStatusId = obj.querystatus;
                dbobj.DateRequested = (obj.dateRequested == "") ? DateTime.Now : Convert.ToDateTime(obj.dateRequested);
                dbobj.RequestedBy = obj.requestedBy;
                //dbobj.ChangeDate = Convert.ToDateTime(obj.changeDate);
                dbobj.Notes = obj.notes;
                dbobj.TradingName = obj.tradingName;
                dbobj.LegalEntitty = obj.legalEntity;
                dbobj.PostCode = obj.postCode;

                DBEntities.Customers.Add(dbobj);
                result = DBEntities.SaveChanges();

                if (dbobj.Id > 0)
                {
                    if (obj.depots.Length > 0)
                    {
                        foreach (var d in obj.depots)
                        {
                            DepotCustomerMapping _obj = new DepotCustomerMapping();
                            _obj.CustomerId = dbobj.Id;
                            _obj.DepotId = d;

                            DBEntities.DepotCustomerMappings.Add(_obj);
                            DBEntities.SaveChanges();
                        }
                    }

                    //update account mapping table for latest customer id
                    CustomerAccountMapping _cust = new CustomerAccountMapping();
                    _cust = DBEntities.CustomerAccountMappings.Where(x => x.AccountCode == dbobj.AccountCode).FirstOrDefault();
                    _cust.CustomerId = dbobj.Id;
                    //DBEntities.DepotCustomerMappings.Add(_obj);
                    DBEntities.SaveChanges();

                }



                if (dbobj.Id != 0)
                        {

                    //before audit entry want to check - which property of object has been updated ;
                    DBEntities.Configuration.ProxyCreationEnabled = false;
                    Customer _prevCustObj = DBEntities.Customers.Select(x=>x).Where(x => x.Id == obj.Id).FirstOrDefault();
                    var _description = GenericMethods.DeepCompare(dbobj, _prevCustObj);
                    AuditLog _obj = new AuditLog();
                            _obj.CustomerId = dbobj.Id;
                            _obj.LastChangeDescription = "Customer -" + dbobj.TradingName + "  has been updated successfully." + System.Environment.NewLine  + _description; 
                        
                            _obj.ModificationDate = DateTime.Now;
                            _obj.ModifiedBy = 1;

                            DBEntities.AuditLogs.Add(_obj);
                            DBEntities.SaveChanges();

                        }
                  //  }
               // }


            }
            catch (Exception ex)
            {
                AuditLog _obj = new AuditLog();
                //_obj.CustomerId = dbobj.Id;
                _obj.LastChangeDescription = "Application Error - " + Convert.ToString(ex);
                _obj.ModificationDate = DateTime.Now;
                _obj.ModifiedBy = 1;

                DBEntities.AuditLogs.Add(_obj);
                DBEntities.SaveChanges();
                throw (ex);
            }

            return result;
        }

        public List<CustomerModel> ListAllCustomer()
        {
            List<CustomerModel> _lst = new List<CustomerModel>();
            var query = from c in DBEntities.Customers
                        join ca in DBEntities.CustomerAccountMappings on c.Id equals ca.CustomerId
                        orderby c.Id descending
                        select c;
            var lst = query.ToList();  //DBEntities.Customers.ToList().OrderByDescending(x => x.Id).Take(100);
            foreach (var obj in lst)
            {
                CustomerModel _custobj = new CustomerModel();
                _custobj.Id = obj.Id;
                _custobj.tradingName = obj.TradingName;
                _custobj.coRegisterationNo = obj.CoRegisterationNo;
                _custobj.accountCode = obj.AccountCode;
                _custobj.address = obj.AddressInfo;
                _custobj.addressline2 = obj.AddressLine2;
                _custobj.addressline3 = obj.AddressLine3;
                _custobj.county = obj.County;
               // _custobj.depotname = obj.Depot.DepotName;
                _custobj.groupname = obj.Group.GroupName;
                _custobj.querystatusvalue = obj.QueryStatu.Question;
                _custobj.statusvalue = obj.Status.Value;
                _custobj.requestedBy = obj.RequestedBy;
                _custobj.dateRequested = DateTime.Parse(Convert.ToString(obj.DateRequested)).ToShortDateString();
                _custobj.legalEntity = obj.LegalEntitty;
                _custobj.notes = obj.Notes;
                _custobj.depotId = Convert.ToInt32(obj.DepotId);
                _custobj.groups = Convert.ToInt32(obj.GroupId);
                _custobj.querystatus = Convert.ToInt32(obj.QueryStatusId);
                _custobj.status = Convert.ToInt32(obj.StatusId);
                _custobj.postCode = obj.PostCode;

                var _depotNames = "";
               
                List<DepotCustomerMappingModel> lstDepotCust = new List<DepotCustomerMappingModel>();
                var _lstDBObj = DBEntities.DepotCustomerMappings.ToList().Where(x => x.CustomerId == obj.Id).Select(x=>x);
               
                    foreach (var oo in _lstDBObj) {
                    DepotCustomerMappingModel objDepCust = new DepotCustomerMappingModel();
                    objDepCust.Id = oo.Id;
                    objDepCust.CustomerId = Convert.ToInt32(oo.CustomerId);
                    objDepCust.DepotId = Convert.ToInt32(oo.DepotId);
                    lstDepotCust.Add(objDepCust);
                    _depotNames = _depotNames + "," + oo.Depot.DepotName;
                    }


                _custobj.listDepotCustomerMapping = lstDepotCust;
                _custobj.depotname = _depotNames;
                _lst.Add(_custobj);

            }
            //(from c in DBEntities.Customers select c);
            //_lst = _obj.ToList();
            return _lst;
        }


        public List<AuditLog> AllAuditLogs(int custId)
        {
            List<AuditLog> _lst = new List<AuditLog>();
            var lst = DBEntities.AuditLogs.Where(c => c.CustomerId == custId).ToList();
            //foreach (var obj in lst)
            //{
            //    AuditLog _custobj = new AuditLog();
            //    _custobj.Id = obj.Id;
            //    _custobj.tradingName = obj.TradingName;
            //    _custobj.coRegisterationNo = obj.CoRegisterationNo;
            //    _custobj.accountCode = obj.AccountCode;
            //    _custobj.address = obj.AddressInfo;
            //    _custobj.depotname = obj.Depot.DepotName;
            //    _custobj.groupname = obj.Group.GroupName;
            //    _custobj.querystatusvalue = obj.QueryStatu.Question;
            //    _custobj.statusvalue = obj.Status.Value;
            //    _custobj.requestedBy = obj.RequestedBy;
            //    _custobj.dateRequested = Convert.ToString(obj.DateRequested);
            //    _custobj.legalEntity = obj.LegalEntitty;
            //    _custobj.notes = obj.Notes;
            //    _custobj.depotId = Convert.ToInt32(obj.DepotId);
            //    _custobj.groups = Convert.ToInt32(obj.GroupId);
            //    _custobj.querystatus = Convert.ToInt32(obj.QueryStatusId);
            //    _custobj.status = Convert.ToInt32(obj.StatusId);
            //    _custobj.postCode = obj.PostCode;

            //    _lst.Add(_custobj);

            //}

            return lst;
        }

        public int GetQueryStatusIdByText(string txt)
        {
            int id = 0;
            if (txt != "") {
                id = DBEntities.QueryStatus.FirstOrDefault(x => x.Question == txt).ID; // Select(x=>x.ID).Where(x=>x.)
            }
            else
            {
                id = DBEntities.QueryStatus.FirstOrDefault(x => x.Question == "N/A").ID;
            }
            return id;
        }

        public int GetDepotIdByText(string txt)
        {
            int id = 0;
            if (txt != "")
            {
                id = DBEntities.Depots.FirstOrDefault(x => x.DepotName == txt).ID; // Select(x=>x.ID).Where(x=>x.)
            }
            else
            {
                id = DBEntities.Depots.FirstOrDefault(x => x.DepotName == "N/A").ID; // Select(x=>x.ID).Where(x=>x.)
            }
            return id;
        }

        public int GetStatusIdByText(string txt)
        {
            int id = 0;
            if (txt != "")
            {
                id = DBEntities.Status.FirstOrDefault(x => x.Value == txt).ID; // Select(x=>x.ID).Where(x=>x.)
            }
            else
            {
                id = DBEntities.Status.FirstOrDefault(x => x.Value == "N/A").ID; // Select(x=>x.ID).Where(x=>x.)
            }
            return id;
        }

        public int GetGroupIdByText(string txt)
        {
            int id = 0;
            if (txt != "")
            {
                id = DBEntities.Groups.FirstOrDefault(x => x.GroupName == txt).ID; // Select(x=>x.ID).Where(x=>x.)
            }
            else
            {
                id = DBEntities.Groups.FirstOrDefault(x => x.GroupName == "N/A").ID;
            }
            return id;
        }

        public int[] GetDepotCustomerMappingId(string txt,DataRow dtDepots)
        {
            int[] ids;
            List<int> _lst = new List<int>();
            if (txt == "VARIOUS  SEE TABLE") {
                if (dtDepots.ItemArray.Length > 0)
                {
                  ids =  GetDepotsFromExcel(dtDepots);
                   return ids;
                }
            }
            else
            {
                txt = (txt.ToUpper()).Trim();
                if (txt != "")
                {
                    int id = DBEntities.Depots.FirstOrDefault(x => x.DepotName == txt).ID; // Select(x=>x.ID).Where(x=>x.)
                    _lst.Add(id);
                }
                else
                {
                    int id = DBEntities.Depots.FirstOrDefault(x => x.DepotName == "N/A").ID; // Select(x=>x.ID).Where(x=>x.)
                    _lst.Add(id);
                }
               
            }
            ids = _lst.ToArray();
            return ids;
        }

        public int?[] GetDepotIds(int customerId)
        {
            int?[] ids;
            List<int?> _lst = new List<int?>();
            _lst = DBEntities.DepotCustomerMappings.Where(x => x.CustomerId == customerId).Select(x=>x.DepotId).ToList(); // Select(x=>x.ID).Where(x=>x.)
            //_lst.Add(id);
            ids = _lst.ToArray();
            return ids;
        }

        public int ValidateAccountCode(string txt)
        {
            int id = 0;
            if (txt != "")
            {

                try
                {
                    var _result = DBEntities.CustomerAccountMappings.Where(x => x.AccountCode == txt).ToList();
                    if (_result.Count > 0) {
                        id = Convert.ToInt32(_result[0].CustomerId);
                    }// Select(x=>x.ID).Where(x=>x.)
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
           
            return id;
        }

        public int[] GetDepotsFromExcel(DataRow dtDepots)
        {
            BIDCustomerDataEntities1 DBEntities1 = new BIDCustomerDataEntities1();
            int[] ids;
            List<int> _lst = new List<int>();
            //get all headers(depot name in list)
            List<string> lstDepots = new List<string>();
            var txt = dtDepots.Table;
            for (int k=14; k<dtDepots.Table.Columns.Count; k++)
            {

                var str = dtDepots.Table.Columns[k].ToString();
                str = (str.ToUpper()).Trim();
                if (dtDepots.ItemArray[k].ToString() != "")
                {
                    var lst = DBEntities1.Depots.Select(x => x).ToList();
                    int id = DBEntities1.Depots.FirstOrDefault(x => x.DepotName == str).ID; // Select(x=>x.ID).Where(x=>x.)
                    _lst.Add(id);
                }
                lstDepots.Add(str);
            }
            //if (dtDepots.ItemArray.Length > 0)
            //{
            //    for(int i=14; i<dtDepots.ItemArray.Length; i++)
            //    {

            //            if (dtDepots.ItemArray[i].ToString() != "")
            //            {
            //                int id = DBEntities.Depots.FirstOrDefault(x => x.DepotName == "Fish Brixham").ID; // Select(x=>x.ID).Where(x=>x.)
            //                _lst.Add(id);
            //            }

            //    }
            //}
            ids = _lst.ToArray();
            return ids;
        }

    }

    public static class GenericMethods
    {
        //generic method
        public static string DeepCompare(this object objnew, object another)
        {
            string msg = "";
            var result = true;
            if (ReferenceEquals(objnew, another))  result = true;
            if ((objnew == null) || (another == null)) result = false;
            //Compare two object's class, return false if they are difference
            if (objnew.GetType() != another.GetType()) result = false;

           
            //Get all properties of obj
            //And compare each other
            foreach (var property in objnew.GetType().GetProperties())
            {
                var objValue = property.GetValue(objnew);
                var anotherValue = property.GetValue(another);
                if (objValue == null && anotherValue == null) { result = false; } else {
                    if (!objValue.Equals(anotherValue)) { result = false;
                        if (property.Name == "Id") { }
                        else if (property.Name == "DepotCustomerMappings") {
                          var tt =  objnew.GetType();
                            List<DepotCustomerMapping> oo = objValue as  List<DepotCustomerMapping>;
                            
                           
                        } else {
                            msg = msg + property.Name + "- value has been changed from (" + anotherValue + ") to (" + objValue + ")" + System.Environment.NewLine;
                        }
                    }
                }
            }

            return msg;
        }

        // remove "this" if not on C# 3.0 / .NET 3.5
        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection props =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < props.Count; i++)
            {
                PropertyDescriptor prop = props[i];
                table.Columns.Add(prop.Name, prop.PropertyType);
            }
            object[] values = new object[props.Count];
            foreach (T item in data)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = props[i].GetValue(item);
                }
                table.Rows.Add(values);
            }
            return table;
        }
    }
}