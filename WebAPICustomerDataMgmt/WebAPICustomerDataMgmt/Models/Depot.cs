﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPICustomerDataMgmt.Models
{
    public class DepotModel
    {

        public int Id { get; set; }

        public string  DepotName { get; set; }
    }

    public class GroupModel
    {

        public int Id { get; set; }

        public string GroupName { get; set; }
    }

    public class statusModel
    {

        public int Id { get; set; }

        public string Value { get; set; }
    }

    public class QueryStatusModel
    {

        public int Id { get; set; }

        public string Question { get; set; }
    }

    public class GetAllSelectValues
    {

        public List<DepotModel>  depot { get; set; }

        public List<GroupModel> group { get; set; }

        public List<statusModel> status { get; set; }

        public List<QueryStatusModel> querystatus { get; set; }
    }
}