﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebAPICustomerDataMgmt.Models
{
    public class CustomerModel
    {
        public int Id { get; set; }
        public string tradingName { get; set; }
        public string companyname { get; set; }
        public string coRegisterationNo { get; set; }
        public string accountCode { get; set; }
        public string address { get; set; }

        public string addressline2 { get; set; }

        public string addressline3 { get; set; }

        public string county { get; set; }

        public string dateRequested { get; set; }
        public string requestedBy { get; set; }
        public string changeDate { get; set; }
        // public List<DepotModel> depots { get; set; }
        public int[] depots;
        public int depotId { get; set; }
        public int groups { get; set; }
        public int status { get; set; }
        public int querystatus { get; set; }
        public string notes { get; set; }
        public string legalEntity { get; set; }
        public string postCode { get; set; }

        public string depotname { get; set; }
        public string groupname { get; set; }
        public string querystatusvalue { get; set; }
        public string statusvalue { get; set; }

        public int DepotCustomerMappingId { get; set; }

        public List<DepotCustomerMappingModel> listDepotCustomerMapping { get; set; }

    }

    public class DepotCustomerMappingModel
    {
        public int Id { get; set; }

        public int CustomerId  { get; set; }

        public int DepotId { get; set; }
    }

}