﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Web;
using System.Web.Http;
using WebAPICustomerDataMgmt.Models;
using ExcelDataReader;
using System.IO;
using WebAPICustomerDataMgmt.DB;
using WebAPICustomerDataMgmt.EntityDB;
using System.Xml;
using System.Xml.Xsl;
using System.ComponentModel;

namespace WebAPICustomerDataMgmt.Controllers
{
    public class ValuesController : ApiController
    {
        string ConnectionString = ConfigurationManager.ConnectionStrings["BIDCustomerData"].ConnectionString;
        // GET api/values
        [Route("Api/Values/GetAllSelectValues")]
        [HttpGet]
        public GetAllSelectValues GetAllSelectValues()
        {

            GetAllSelectValues objListMain = new GetAllSelectValues();
            SqlConnection cnn = new SqlConnection(ConnectionString);

            DataTable dt = new DataTable();
            cnn.Open();
            string sqlQuery = "Select Id, DepotName from [dbo].[Depot]";
            SqlCommand sqlComm = new SqlCommand(sqlQuery, cnn);
            sqlComm.CommandType = CommandType.Text;
            SqlDataAdapter da = new SqlDataAdapter(sqlComm);
            da.Fill(dt);

            var objList = ConvertToList<DepotModel>(dt);
            objListMain.depot = objList;
            cnn.Close();

            //----groups---//

            DataTable dt1 = new DataTable();
            cnn.Open();
            string sqlQuery1 = "Select Id, GroupName from [dbo].[Group]";
            SqlCommand sqlComm1 = new SqlCommand(sqlQuery1, cnn);
            sqlComm1.CommandType = CommandType.Text;
            SqlDataAdapter da1 = new SqlDataAdapter(sqlComm1);
            da1.Fill(dt1);

            var objListGrp = ConvertToList<GroupModel>(dt1);
            objListMain.group = objListGrp;
            cnn.Close();

            //-----status------------//

            DataTable dt2 = new DataTable();
            cnn.Open();
            string sqlQuery2 = "Select Id, Value from [dbo].[Status]";
            SqlCommand sqlComm2 = new SqlCommand(sqlQuery2, cnn);
            sqlComm2.CommandType = CommandType.Text;
            SqlDataAdapter da2 = new SqlDataAdapter(sqlComm2);
            da2.Fill(dt2);

            var objListSts = ConvertToList<statusModel>(dt2);
            objListMain.status = objListSts;
            cnn.Close();

            //----querystatus-------//

            DataTable dt3 = new DataTable();
            cnn.Open();
            string sqlQuery3 = "Select Id, Question from [dbo].[QueryStatus]";
            SqlCommand sqlComm3 = new SqlCommand(sqlQuery3, cnn);
            sqlComm3.CommandType = CommandType.Text;
            SqlDataAdapter da3 = new SqlDataAdapter(sqlComm3);
            da3.Fill(dt3);

            var objListQuerySts = ConvertToList<QueryStatusModel>(dt3);
            objListMain.querystatus = objListQuerySts;
            cnn.Close();


            return objListMain;
        }

        [Route("Api/Values/GetAllCustomers")]
        [HttpGet]
        public List<CustomerModel> GetAllCustomer()
        {
            List<CustomerModel> _list = new List<CustomerModel>();
            try
            {
                DBClass _db = new DBClass();
                _list = _db.ListAllCustomer();
                return _list;
            }
            catch (Exception ex)
            {

                BIDCustomerDataEntities1 DBEntities = new BIDCustomerDataEntities1();
                AuditLog _obj = new AuditLog();
                _obj.CustomerId = -1;
                _obj.LastChangeDescription = "Customer's list not loaded ::  " + ex;
                _obj.ModificationDate = DateTime.Now;
                _obj.ModifiedBy = 1;

                DBEntities.AuditLogs.Add(_obj);
                DBEntities.SaveChanges();
                return _list;
            }
        }

        // POST api/values
        [Route("Api/Values/SaveUpdateCustomer")]
        [HttpPost]
        public object SaveCustomerData(CustomerModel custObj)
        {
            DBClass _db = new DBClass();
            if (custObj.Id > 0) {
                _db.UpdateCustomer(custObj);

            }
            else {
                _db.SaveCustomer(custObj);
            }
            return new ResultVM { Status = "Success", Message = "Ok" };
        }

        //[Route("Api/Values/customers")]
        //[HttpPost]
        //public IHttpActionResult SaveCustomer(string name, string email, string tel)
        //{
        //    return Ok("succes");
        //}

        [Route("Api/Values/GetAuditLog")]
        [HttpGet]
        public List<AuditLog> GetAuditLog(int customerId)
        {
            // List<CustomerModel> _list = new List<CustomerModel>();
            DBClass _db = new DBClass();
            var _list = _db.AllAuditLogs(customerId);
            return _list;
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        [Route("Api/Values/UploadExcel")]
        [HttpPost]
        public string ExcelUpload()
        {
            int countEdit = 0;
            int countAdd = 0;
            DBClass _db = new DBClass();
            string message = "";
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;
            if (httpRequest.Files.Count > 0)
            {
                HttpPostedFile file = httpRequest.Files[0];
                Stream stream = file.InputStream;

                IExcelDataReader reader = null;

                if (file.FileName.EndsWith(".xls"))
                {
                    reader = ExcelReaderFactory.CreateBinaryReader(stream);
                }
                else if (file.FileName.EndsWith(".xlsx"))
                {
                    reader = ExcelReaderFactory.CreateOpenXmlReader(stream);
                }
                else
                {
                    message = "This file format is not supported";
                }

                DataSet excelRecords = reader.AsDataSet(new ExcelDataSetConfiguration
                {
                    ConfigureDataTable = _ => new ExcelDataTableConfiguration
                    {
                        UseHeaderRow = true // Use first row is ColumnName here :D
                    }
                });

                reader.Close();
                int output = 0;// objEntity.SaveChanges();
                var finalRecords = excelRecords.Tables[0];
                for (int i = 0; i < finalRecords.Rows.Count; i++)
                {

                    CustomerModel objCustomer = new CustomerModel();
                    objCustomer.querystatus = _db.GetQueryStatusIdByText(finalRecords.Rows[i][0].ToString());
                    objCustomer.status = _db.GetStatusIdByText(finalRecords.Rows[i][1].ToString());
                    objCustomer.groups = _db.GetGroupIdByText(finalRecords.Rows[i][2].ToString());
                    objCustomer.accountCode = finalRecords.Rows[i][3].ToString();
                    objCustomer.tradingName = finalRecords.Rows[i][4].ToString();
                    objCustomer.legalEntity = finalRecords.Rows[i][5].ToString();
                    objCustomer.postCode = finalRecords.Rows[i][6].ToString();
                    objCustomer.coRegisterationNo = finalRecords.Rows[i][7].ToString();
                    objCustomer.address = finalRecords.Rows[i][8].ToString();
                    // objCustomer.addressline2 = obj.addressline2;
                    // objCustomer.addressline3 = obj.addressline3;
                    // objCustomer.county = obj.county;
                    objCustomer.depots = _db.GetDepotCustomerMappingId(finalRecords.Rows[i][9].ToString(), finalRecords.Rows[i]);
                    objCustomer.dateRequested = Convert.ToString(finalRecords.Rows[i][10].ToString());
                    objCustomer.requestedBy = finalRecords.Rows[i][11].ToString();
                    //objCustomer.ChangeDate = finalRecords.Rows[i][12].ToString();
                    objCustomer.notes = finalRecords.Rows[i][13].ToString();

                    if (finalRecords.Rows[i][3].ToString() != "")
                    {
                        var _check = _db.ValidateAccountCode(finalRecords.Rows[i][3].ToString());
                        if (_check > 0)
                        {
                            //update records on the basis of spreadsheet data
                            countEdit++;
                            objCustomer.Id = _check;
                            output = _db.UpdateCustomer(objCustomer);
                        }
                        else
                        {
                            countAdd++;
                            output = _db.SaveCustomer(objCustomer);
                        }
                    }
                }


                if (output > 0)
                {
                    message = countEdit + " no. of records have been updated. \n " + countAdd + " no. of records have been added";
                }
                else
                {
                    message = "Failed";
                }

            }

            else
            {
                result = Request.CreateResponse(HttpStatusCode.BadRequest);
            }
            //}
            return message;
        }

        [Route("Api/Values/ExportExcellast")]
        [HttpPost]
        public void ExportToExcellast()
        {
            //XmlDataDocument xmlDataDoc = new XmlDataDocument(ds);
            //XslTransform xt = new XslTransform();
            //StreamReader reader = new StreamReader(typeof(WorkbookEngine).Assembly.GetManifestResourceStream(typeof(WorkbookEngine), "Excel.xsl"));
            //XmlTextReader xRdr = new XmlTextReader(reader);
            //xt.Load(xRdr, null, null);
            //StringWriter sw = new StringWriter();
            //xt.Transform(xmlDataDoc, null, sw, null);
            //StreamWriter myWriter = new StreamWriter(path + “\\Report.xls”);
            //myWriter.Write(sw.ToString());
            //myWriter.Close();
            DBClass _db = new DBClass();
            //GenericMethods _gm = new GenericMethods();
            var dt = GenericMethods.ToDataTable(_db.ListAllCustomer());
            Microsoft.Office.Interop.Excel.Application excel;
            Microsoft.Office.Interop.Excel.Workbook excelworkBook;
            Microsoft.Office.Interop.Excel.Worksheet excelSheet;
            Microsoft.Office.Interop.Excel.Range excelCellrange;

            excel = new Microsoft.Office.Interop.Excel.Application();
            // for making Excel visible  
            excel.Visible = false;
            excel.DisplayAlerts = false;
            // Creation a new Workbook  
            excelworkBook = excel.Workbooks.Add(Type.Missing);
            // Workk sheet  
            excelSheet = (Microsoft.Office.Interop.Excel.Worksheet)excelworkBook.ActiveSheet;
            excelSheet.Name = "Test work sheet";

            excelSheet.Cells[1, 1] = "Sample test data";
            excelSheet.Cells[1, 2] = "Date : " + DateTime.Now.ToShortDateString();

            // now we resize the columns  
            excelCellrange = excelSheet.Range[excelSheet.Cells[1, 1], excelSheet.Cells[100, dt.Columns.Count]];
            excelCellrange.EntireColumn.AutoFit();
            Microsoft.Office.Interop.Excel.Borders border = excelCellrange.Borders;
            border.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
            border.Weight = 2d;

        }

        [Route("Api/Values/ExportExcel")]
        [HttpPost]
        public void ExportToExcel()
        {
            var excelPath = "C:\\Users\\kverma\\Documents\\Demo.xls";
            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            object misValue = System.Reflection.Missing.Value;

            try
            {
                //Previous code was referring to the wrong class, throwing an exception
                xlApp = new Microsoft.Office.Interop.Excel.Application();
                xlWorkBook = xlApp.Workbooks.Add(misValue);
                xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

                DBClass _db = new DBClass();
                var dt = GenericMethods.ToDataTable(_db.ListAllCustomer());
                for (int i = 0; i <= dt.Rows.Count - 1; i++)
                {
                    for (int j = 0; j <= dt.Columns.Count - 1; j++)
                    {
                        xlWorkSheet.Cells[i + 1, j + 1] = dt.Rows[i].ItemArray[j].ToString();
                    }
                }

                xlWorkBook.SaveAs(excelPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, misValue, misValue, misValue, misValue, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, misValue, misValue, misValue, misValue, misValue);
                xlWorkBook.Close(true, misValue, misValue);
                xlApp.Quit();

                releaseObject(xlApp);
                releaseObject(xlWorkBook);
                releaseObject(xlWorkSheet);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void releaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch

            {
                obj = null;
            }
            finally
            {
                GC.Collect();
            }
        }
    


        /*Generic Method*/

        public static List<T> ConvertToList<T>(DataTable dt)
        {
            var columnNames = dt.Columns.Cast<DataColumn>()
                    .Select(c => c.ColumnName)
                    .ToList();
            var properties = typeof(T).GetProperties();
            return dt.AsEnumerable().Select(row =>
            {
                var objT = Activator.CreateInstance<T>();
                foreach (var pro in properties)
                {
                    if (columnNames.Contains(pro.Name))
                    {
                        if (pro.Name == "CustomerDeliveryDate")
                        {
                            string dateformat = Convert.ToDateTime(row[pro.Name]).ToShortDateString();
                            PropertyInfo pI = objT.GetType().GetProperty(pro.Name);
                            pro.SetValue(objT, Convert.ChangeType(dateformat, pI.PropertyType));
                        }
                        else
                        {

                            PropertyInfo pI = objT.GetType().GetProperty(pro.Name);
                            pro.SetValue(objT, row[pro.Name] == DBNull.Value ? null : Convert.ChangeType(row[pro.Name], pI.PropertyType));
                        }
                    }
                }
                return objT;
            }).ToList();
        }

       

    }

    internal class ResultVM
    {
        public string Status { get; set; }
        public string Message { get; set; }
    }
}
