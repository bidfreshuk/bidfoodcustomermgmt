import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import { FormBuilder, FormGroup, FormArray, FormControl, Validators  } from '@angular/forms';
import { FileUploader, FileSelectDirective } from 'ng2-file-upload/ng2-file-upload';

import { Router } from '@angular/router';  
import { ValuesService } from '../service/values.service';

import { Depot } from '../models/depot';
import { Status } from '../models/status';
import { Querystatus } from '../models/querystatus';
import { Group } from '../models/group';

import { UploadDoc } from '../models/upload-doc';
import { Observable } from 'rxjs';
import { Customer } from '../models/customer';
import { DatePipe } from '@angular/common'
import { SelectItem } from 'primeng/api/selectitem';
import { NgxSpinnerService } from "ngx-spinner";  



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  closeResult: string;
  modalOptions: NgbModalOptions;
  
  form: FormGroup;
  uploadform: FormGroup;
  depots = [];
  groups = [];
  status = [];
  querystatus = [];
  allCustomers: Customer[] ;
  cols: any[];
 // accountcodes: SelectItem[];
  depotSelected = [];

  isLoaded = false;

  searchText;
  submitted = false;
  customerDetails;
  auditDetails: Observable<any[]>;
  customerDateRequested;

  optionAccountCodes = [];
  optionTradingName = [];
  optionCoRegNo = [];
  optionAddress = [];
  optionPostcode = [];

  daterequested;

  constructor(private formBuilder: FormBuilder,
    private modalService: NgbModal, private router: Router, private ValuesService: ValuesService, private SpinnerService: NgxSpinnerService
  ) {
    this.modalOptions = {
      backdrop: 'static',
      backdropClass: 'customBackdrop'
    };
    this.form = this.formBuilder.group({
      depots: ['', Validators.required], groups: ['', Validators.required], status: ['', Validators.required], querystatus: ['', Validators.required], companyname: [''],
      coRegisterationNo: [''], accountCode: ['', Validators.required], address: ['', Validators.required], dateRequested: ['', Validators.required],
      requestedby: ['', Validators.required], changeDate: [''], notes: [''], tradingName: ['', Validators.required], legalEntity: [''], postcode: ['', Validators.required], id: [''],
      addressline2:[''],addressline3: [''],county:['']
    })
    this.getallSelectValues();

    this.uploadform = this.formBuilder.group({
      name: [''],
      avatar:[null]
    })

    this.getAllCustomer();
  }

  

  ngOnInit() {
    this.cols = [
      { field: 'accountCode', header: 'Account Code' },
      { field: 'tradingName', header: 'Trading Name' },
      { field: 'coRegisterationNo', header: 'Co.Reg.No' },
      { field: 'address', header: 'Address' },
      { field: 'postCode', header: 'Postcode' }
    ];
   
 
    
    
    //this.accountcodes = [
    //  //{ label: 'All Account Codes', value: null },
    //  { label: 'ACC678', value: 'ACC678' }
    //];

    
  }


  // convenience getter for easy access to form fields
  get f() { return this.form.controls; }
  /**
   * Model popup 
   * @param content
   */
  open(content) {
    this.modalService.open(content, {
      size: 'xl', backdrop: 'static', windowClass:'custom-modal' }).result.then((result) => {
      this.closeResult = 'Closed with: ${result}';
    }, (reason) => {
      this.closeResult = 'Dismissed ${this.getDismissReason(reason)}';
      });
  
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return 'with: ${reason}';
    }
  }

  /*model popup end */

  /*Binding drop downs for customer form */

  getallSelectValues(): any {
    this.ValuesService.getAllSelectValuesService().subscribe(data => {
      if (data) {
        this.depots = data.depot;
        this.groups = data.group;
        this.status = data.status;
        this.querystatus = data.querystatus;
      }
    });
  }

  getselecteddepotValues() {
    this.depotSelected;
  }

/*----------------------------------- */

/*Bind customer grid */
  getAllCustomer() {
    this.SpinnerService.show();  
    let customerobj = this.ValuesService.getListOfAllCustomer();
    customerobj.subscribe(cust => {
      this.allCustomers = cust,
      this.bindingpMultiselectoptiondynamic(cust);
      this.SpinnerService.hide(); 

    });
  }

  bindingpMultiselectoptiondynamic(t) {
    //for account optitons
    t.forEach(obj => { this.optionAccountCodes.push({ label: obj.accountCode, value: obj.accountCode }) }); // from existing customer response gathering all accountcodes
    const newset = Array.from(new Set(this.optionAccountCodes.map(x => x.value))).map(value => {  // getting distinct account codes for binding p-multiselect options dynamically
      return {
        value: value,
        label: this.optionAccountCodes.find(s => s.value == value).label
      }
    });
   
    this.optionAccountCodes = newset;

    //for trading name
    t.forEach(obj => { this.optionTradingName.push({ label: obj.tradingName, value: obj.tradingName }) }); // from existing customer response gathering all accountcodes
    const newset1 = Array.from(new Set(this.optionTradingName.map(x => x.value))).map(value => {  // getting distinct account codes for binding p-multiselect options dynamically
      return {
        value: value,
        label: this.optionTradingName.find(s => s.value == value).label
      }
    });
  
    this.optionTradingName = newset1;


    //for co reg no
    t.forEach(obj => { this.optionCoRegNo.push({ label: obj.coRegisterationNo, value: obj.coRegisterationNo }) }); // from existing customer response gathering all accountcodes
    const newset2 = Array.from(new Set(this.optionCoRegNo.map(x => x.value))).map(value => {  // getting distinct account codes for binding p-multiselect options dynamically
      return {
        value: value,
        label: this.optionCoRegNo.find(s => s.value == value).label
      }
    });
   
    this.optionCoRegNo = newset2;

    //for postcode
    t.forEach(obj => { this.optionPostcode.push({ label: obj.postCode, value: obj.postCode }) }); // from existing customer response gathering all accountcodes
    const newset3 = Array.from(new Set(this.optionPostcode.map(x => x.value))).map(value => {  // getting distinct account codes for binding p-multiselect options dynamically
      return {
        value: value,
        label: this.optionPostcode.find(s => s.value == value).label
      }
    });
   
    this.optionPostcode = newset3;

  }



/*----------------- */

  CustomerEdit(Customer: Customer) {
   // alert(Customer.id + "...." + Customer.dateRequested);
    //this.depotSelected = [''];
    
    this.Reset();
    
    if (Customer.listDepotCustomerMapping.length > 0) {
      //alert(Customer.listDepotCustomerMapping.length);
      for (var i = 0; i < Customer.listDepotCustomerMapping.length; i++) {
        //alert(Customer.listDepotCustomerMapping[i].depotId);
        this.depotSelected.push(Customer.listDepotCustomerMapping[i].depotId);
      }
    }

    this.form.controls['tradingName'].setValue(Customer.tradingName);   
    this.form.controls['groups'].setValue(Customer.groups);
    this.form.controls['coRegisterationNo'].setValue(Customer.coRegisterationNo);
    this.form.controls['accountCode'].setValue(Customer.accountCode);
    this.form.controls['address'].setValue(Customer.address);
    this.form.controls['addressline2'].setValue(Customer.addressline2);
    this.form.controls['addressline3'].setValue(Customer.addressline3);
    this.form.controls['county'].setValue(Customer.county);
    this.form.controls['postcode'].setValue(Customer.postCode);
    //alert(Customer.dateRequested);
    this.daterequested = Customer.dateRequested;
  //  this.form.controls['dateRequested'].setValue("2019/12/01");
   // this.customerDateRequested = new Date(Customer.dateRequested);

    //let dp = new DatePipe(navigator.language);
    //let p = 'dd/MM/yyyy'; // YYYY-MM-DD
    //let dtr = dp.transform(new Date(Customer.dateRequested).toISOString().substring(0, 10), p);
    //this.form.setValue({ dateRequested: dtr }); 
    //const Year = Number(this.datePipe.transform(Customer.dateRequested, 'yyyy'));
    //const Month = Number(this.datePipe.transform(Customer.dateRequested, 'MM'));
    //const Day = Number(this.datePipe.transform(Customer.dateRequested, 'dd'));
    //this.form.controls['dateRequested'].setValue({
    //  year: Year,
    //  month: Month,
    //  day: Day
    //});

    this.form.controls['requestedby'].setValue(Customer.requestedBy);
    this.form.controls['status'].setValue(Customer.status);
    this.form.controls['querystatus'].setValue(Customer.querystatus);
    this.form.controls['legalEntity'].setValue(Customer.legalEntity);
    this.form.controls['notes'].setValue(Customer.notes);
    this.form.controls['id'].setValue(Customer.id);  
  }

  /**
   * Customer view detail and their popup model
   * @param Customer
   */
  CustomerView(Customer: Customer) {
    this.customerDetails = Customer;
  }


  openView(content) {
    this.modalService.open(content, { size: 'xl', backdrop: 'static', windowClass: 'custom-modal'}).result.then((result) => {
      this.closeResult = 'Closed with: ${result}';
    }, (reason) => {
        this.closeResult = 'Dismissed ${this.getDismissReasonview(reason)}';
    });
  }

  private getDismissReasonview(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return 'with: ${reason}';
    }
  }

/** end */

/*** Customer last changes history : audit log and popup model */
  AuditView(Id: number) {
    this.auditDetails = this.ValuesService.getAuditLogByCustomerId(Id);
  }

  openAuditView(content) {
    this.modalService.open(content, this.modalOptions).result.then((result) => {
      this.closeResult = 'Closed with: ${result}';
    }, (reason) => {
        this.closeResult = 'Dismissed ${this.getDismissReasonAuditview(reason)}';
    });
  }

  private getDismissReasonAuditview(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return 'with: ${reason}';
    }
  }
  /** */

  submit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.form.invalid) {
      return;
    }

    this.ValuesService.postCustomerData(this.form.value).subscribe(
      () => {
       // this.dataSaved = true;
       // this.massage = 'Record saved Successfully';
        this.getAllCustomer();
        this.Reset();
        //this.StudentId = "0";
      });  ;
  }

  Reset() {
    this.submitted = false;
    this.form.reset();
    this.depotSelected=[];
  } 



  @ViewChild('fileInput', {static:false}) fileInput;  
  //uploader
  uploadFile() {
   // this.SpinnerService.show(); 
    let formData = new FormData();
    formData.append('upload', this.fileInput.nativeElement.files[0])  
    this.ValuesService.UploadExcel(formData).subscribe(response => {
      if (response == "Failed") {
        alert("Something went wrong with excelsheet");
      } else {
        alert(response);
        this.fileInput.nativeElement.value = null;
        this.getAllCustomer();
      }
    },
      (err) => alert(err));
  }

  exporttoExcel() {
   
    this.ValuesService.ExportExcel().subscribe(response => {
      alert(response);
    },
      (err) => alert(err));
  }


  //listToUpload: UploadDoc = { Id: 0, FileToUpload: null };
  //handleFileInput(files: FileList) {
  
  // this.listToUpload.FileToUpload = files.item(0);
  //  this.listToUpload = files.item(0);
  //}
  //public uploadList() {
  //  this.disableSubmit = true;
  //  this.rastetService.uploadFile(this.listToUpload, this.userInfo.Token).subscribe((result: string) => {
  //    this.thisDialogRef.close('close');
  //    this.disableSubmit = false;
  //  },
  //    error => {
  //      if (error instanceof HttpErrorResponse) {

  //      }
  //      else {
  //      }
  //      this.spinnerService.hide();
  //      this.disableSubmit = false;
  //    });
  //}

  //uploadFiletest(event) {
  //  const file = (event.target as HTMLInputElement).files[0];
  //  alert(file);
  //  this.uploadform.patchValue({
  //    avatar: file
  //  });
  //  this.uploadform.get('avatar').updateValueAndValidity()
  //}


  //submitForm() {
  //  var formData: any = new FormData();
  //  //formData.append("name", this.uploadform.get('name').value);
  //  formData.append("avatar", this.uploadform.get('avatar').value);
  //  alert(this.uploadform.get('avatar').value)
  // // this.http.post('http://localhost:4000/api/create-user', formData).subscribe(
  //  this.ValuesService.UploadExcel(formData);
  //}



  //*function for binding p-multislect options dyanamically*//

  bindpMultiselectOptions(arrobj) {
    //let obj:[] =  {
    //  label: String;
    //  value: string;

    //}
    if (arrobj.length > 0) {

    }

  }

}

