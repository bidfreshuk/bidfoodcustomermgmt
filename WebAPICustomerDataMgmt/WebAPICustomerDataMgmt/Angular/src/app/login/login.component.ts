import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';    
import { LoginService } from '../service/login.service';    
import { FormBuilder, FormsModule, FormGroup } from '@angular/forms';    
import { User } from '../models/user';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginform: FormGroup;
  model: User;    
  errorMessage:string; 
  constructor(private router: Router, private LoginService: LoginService, private formBuilder: FormBuilder) {
    this.loginform = this.formBuilder.group({
     Email: [''],Password:['']
    })
  }

  ngOnInit() {
  }
  login() {
    debugger;
    this.LoginService.authenticatelogin(this.loginform.value).subscribe(
      success => {  this.router.navigate(['Dashboard']);} ,//alert("Done"),
      error => alert(error)
    );
  };    

}
