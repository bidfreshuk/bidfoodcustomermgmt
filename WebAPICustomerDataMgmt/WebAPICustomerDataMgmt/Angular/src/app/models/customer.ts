import { Depotcustomermapping } from './depotcustomermapping';

export class Customer {
  id: number;
  depots: number;
  groups: number;
  companyname: string;
  coRegisterationNo: string;
  accountCode: string;
  address: string;
  addressline2: string;
  addressline3: string;
  county: string;
  dateRequested: string;
  requestedBy: string;
  changeDate: string;
  status: number;
  querystatus: number;
  notes: string;
  tradingName: string;
  legalEntity: string;
  postCode: string;
  depotname: string;
  groupname: string;
  querystatusvalue: string;
  statusvalue: string;
  depotId: number;
  listDepotCustomerMapping: Depotcustomermapping[];
}
