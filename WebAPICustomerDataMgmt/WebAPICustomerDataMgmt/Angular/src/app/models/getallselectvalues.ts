
import { Depot } from '../models/depot';
import { Status } from '../models/status';
import { Querystatus } from '../models/querystatus';
import { Group } from '../models/group';

export class Getallselectvalues {
  public depot: Depot[];
  public status: Status[];
  public querystatus: Querystatus[];
  public group: Group[];


}

