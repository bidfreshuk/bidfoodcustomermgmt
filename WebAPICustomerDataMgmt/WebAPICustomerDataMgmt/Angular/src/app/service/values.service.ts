import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Getallselectvalues } from '../models/getallselectvalues';
import { Observable } from 'rxjs';
import { Customer } from '../models/customer';


@Injectable({
  providedIn: 'root'
})
export class ValuesService {
  Url: string;
  token: string;
  header: any

  //public depot: Depot[];

  constructor(private http: HttpClient) {
    this.Url = 'http://bflhlwsvr0005/CustomerAPI/';
    //this.Url = 'https://localhost:44311/';
    const headerSettings: { [name: string]: string | string[]; } = {};
    this.header = new HttpHeaders(headerSettings);
  }

  public select_API = this.Url + 'Api/Values/GetAllSelectValues/';
  //getDepots(model: any) {
  //return this.http.get<any>(this.depot_API).subscribe(result => { this.depot = result; },error=> console.error(error));

  //} 
  //getAllSelectValues() {
  //const headers = new HttpHeaders({ 'test': 'test1' });
  //return this.http.get<any>(this.depot_API, { headers });
  //}

  getAllSelectValuesService(): Observable<Getallselectvalues> {
    return this.http
      .get<Getallselectvalues>(this.Url + 'Api/Values/GetAllSelectValues/');
      //.map(res => res.shows);
  }

  getListOfAllCustomer(): Observable<Customer[]> {
    return this.http.get<Customer[]>(this.Url + 'Api/Values/GetAllCustomers/');
  }

  getAuditLogByCustomerId(Id: number): Observable<any[]> {
    return this.http.get<any[]>(this.Url + 'Api/Values/GetAuditLog?customerId=' + Id);
  }



  public formPost_API = this.Url + 'Api/Values/SaveUpdateCustomer';
  public postCustomerData(customer: Customer) {
    
    return this.http.post(this.Url + 'Api/Values/SaveUpdateCustomer', customer);
   
  }


  errorHandler(errorHandler: any): import("rxjs").OperatorFunction<Object, unknown> {
        throw new Error("Method not implemented.");
    }

  

  public upload_Api = this.Url + 'Api/Values/UploadExcel'
  UploadExcel(formData: FormData) {
    let headers = new HttpHeaders();

    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');

    const httpOptions = { headers: headers };

    return this.http.post(this.Url + 'Api/Values/UploadExcel', formData, httpOptions);
  }

  ExportExcel() {
    let headers = new HttpHeaders();

    headers.append('Content-Type', 'multipart/form-data');
    headers.append('Accept', 'application/json');

    const httpOptions = { headers: headers };

    return this.http.post(this.Url + 'Api/Values/ExportExcel', httpOptions);
  }  

}
