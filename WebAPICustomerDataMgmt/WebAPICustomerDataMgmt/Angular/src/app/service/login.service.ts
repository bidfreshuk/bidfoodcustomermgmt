import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http'; 
//import {Observable} from 'rxjs/Rx';
import { User } from '../models/User';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  ApiUrl:string;
  token:string;
  header:any;


  
  constructor(private http: HttpClient) { 
    this.ApiUrl = 'http://bflhlwsvr0005/CustomerAPI';
    //this.ApiUrl = 'https://localhost:44311';
    const headerSettings: {[name: string]: string | string[]; } = {};  
    this.header = new HttpHeaders(headerSettings);  
  }
  public login_API = 'https://localhost:44311/Api/Login/Authenticate/';
  authenticatelogin(model: User) {
    return this.http.post<any>(this.ApiUrl + '/Api/Login/Authenticate/', model,{headers : this.header});
    
  }
}
